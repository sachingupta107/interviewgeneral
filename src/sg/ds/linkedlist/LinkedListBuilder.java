package sg.ds.linkedlist;

public class LinkedListBuilder {

	public static Node<Integer> getIntegerList() {
		return getIntegerList(5, 10);
	}

	public static Node<Integer> getIntegerList(int len, int range) {
		if (len <= 0) return null;
		
		Node<Integer> root = null;
		int data = (int)(Math.random()*(range));
		root = new Node<>(data);
		
		Node<Integer> node = root;
		for (int i = 0; i < (len - 1); i++) {
			data = (int)(Math.random()*(range));
			node.next = new Node<>(data);
			node = node.next;
		}
		return root;
	}
	
	public static Node<Integer> buildIntegerListFromString(String values, String sep) {
		String[] valArr = values.split(sep);
		Node<Integer> ret = new Node<>(Integer.parseInt(valArr[0].trim()));
		Node<Integer> root = ret;
		for (int i = 1; i < valArr.length; i++) {
			ret.next = new Node<>(Integer.parseInt(valArr[i].trim()));
			ret = ret.next;
		}
		return root;
	}

}

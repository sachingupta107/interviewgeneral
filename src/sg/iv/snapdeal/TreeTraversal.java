package sg.iv.snapdeal;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import sg.ds.btree.Node;
import sg.ds.btree.PrettyPrinterBTree;
import sg.ds.btree.SampleBTrees;

public class TreeTraversal {

	public static void inOrderRec(Node<?> root) {
		Node<?> node = root;
		if (node == null)
			return;
		inOrderRec(node.left);
		System.out.print(node + " ");
		inOrderRec(node.right);
	}

	public static void preOrderRec(Node<?> root) {
		Node<?> node = root;
		if (node == null)
			return;
		System.out.print(node + " ");
		preOrderRec(node.left);
		preOrderRec(node.right);
	}

	public static void postOrderRec(Node<?> root) {
		Node<?> node = root;
		if (node == null)
			return;

		postOrderRec(node.left);
		postOrderRec(node.right);
		System.out.print(node + " ");
	}

	//-------------Loop using Map
	
	public static void inOrderLoopMap(Node<?> root) {

		Stack<Node<?>> stk = new Stack<>();
		Map<Node<?>, Boolean> processedNodes = new HashMap<>();
		stk.add(root);

		while (!stk.isEmpty()) {
			// System.out.println("stk: "+stk);
			Node<?> node = stk.pop();
			if (node == null)
				break;
			if ((node.left != null) && (processedNodes.get(node.left) == null)) {
				stk.add(node);
				stk.add(node.left);
			} else {
				System.out.print(node + " ");
				processedNodes.put(node, true);
				if (node.right != null)
					stk.add(node.right);
			}
		}
	}

	public static void preOrderLoopMap(Node<?> root) {

		if (root == null)
			return;

		Stack<Node<?>> stk = new Stack<>();
		Map<Node<?>, Boolean> processedNodes = new HashMap<>();

		stk.add(root);

		while (!stk.isEmpty()) {
			Node<?> node = stk.pop();
			System.out.print(node + " ");
			processedNodes.put(node, true);
			if ((node.right != null) && (processedNodes.get(node.right) == null)) {
				stk.add(node.right);
			}
			if ((node.left != null) && (processedNodes.get(node.left) == null)) {
				stk.add(node.left);
			}
		}
	}

	public static void postOrderLoopMap(Node<?> root) {
		if (root == null)
			return;

		Stack<Node<?>> stk = new Stack<>();
		Map<Node<?>, Boolean> processedNodes = new HashMap<>();
		stk.add(root);
		boolean processNode = true;
		while (!stk.isEmpty()) {
			Node<?> node = stk.pop();
			processNode = true;
			if (node.right != null && processedNodes.get(node.right) == null) {
				stk.add(node);
				stk.add(node.right);
				processNode = false;
			}
			
			if (node.left != null && processedNodes.get(node.left) == null) {
				if (node.right == null) {
					stk.add(node);
				}
				stk.add(node.left);
				processNode = false;
			}

			if (processNode) {
				processedNodes.put(node, true);
				System.out.print(node + " ");
			}
		}

	}

	//-----------------Loop without map
	
	public static void inOrderLoop2(Node<?> root) {

		if (root == null)
			return;

		Stack<Node<?>> stk = new Stack<>();
		Node<?> currNode = root;
		do {

			while (currNode != null) {
				stk.add(currNode);
				System.out.println("\nstk add currnode:");
				System.out.println(stk);
				currNode = currNode.left;
			}

			Node<?> nodePop = stk.pop();
			System.out.print(nodePop + " ");
			if (nodePop.right != null) {
				stk.add(nodePop.right);
				System.out.println("\nstk add rightnode:");
				System.out.println(stk);
				currNode = nodePop.right.left;
			}

		} while (!stk.isEmpty());

	}

	public static void inOrderLoop21(Node<?> root) {

		if (root == null)
			return;

		Stack<Node<?>> stk = new Stack<>();
		stk.add(root);
		Node<?> lNode = root.left;

		do {
			// System.out.println("lNode: "+lNode);
			// System.out.println("Stack:");System.out.println(stk);

			if (lNode != null) {
				stk.add(lNode);
				lNode = lNode.left;
				continue;
			}

			// lNode = null
			// currNode has no left child pending
			Node<?> currNode = stk.pop();
			System.out.print(currNode + " ");

			Node<?> rNode = currNode.right;
			if (rNode != null) {
				stk.add(rNode);
				lNode = rNode.left;
				continue;
			}

		} while (!stk.isEmpty());

	}

	public static void preOrderLoop2(Node<?> root) {

		if (root == null)
			return;

		Stack<Node<?>> stk = new Stack<>();
		Node<?> currNode = root;
		stk.add(currNode);
		do {
			System.out.println("\nstk:");
			System.out.println(stk);
			currNode = stk.pop();
			// if (currNode == null) {
			//
			// }

			System.out.print(currNode + " ");
			Node<?> lNode = currNode.left;
			Node<?> rNode = currNode.right;
			// currNode = null;
			if (rNode != null) {
				stk.add(rNode);
				// rNode = rNode.right;
				// currNode = rNode;

			}

			if (lNode != null) {
				stk.add(lNode);
				// lNode = lNode.left;
				// currNode = lNode;
			}

			// else {
			// currNode = null;
			// }

		} while (!stk.isEmpty());

	}

	public static void main(String[] args) {
		Node<Integer> root = SampleBTrees.test3();
		PrettyPrinterBTree.printBTreeNode(root);

		// ----------------------------------------INORDER
		// System.out.println("in order recursive");
		// inOrderRec(root);
		// System.out.println("");
		//
		// System.out.println("in order loop map");
		// inOrderLoopMap(root);
		// System.out.println("");
		//
		// System.out.println("in order loop 2");
		// inOrderLoop2(root);
		// System.out.println("");
		//
		// System.out.println("in order loop 21");
		// inOrderLoop21(root);
		// System.out.println("");
		// ---------------------------------------PREORDER
		// System.out.println("pre order recursive");
		// preOrderRec(root);
		// System.out.println("");
		//
		// System.out.println("pre order loop map");
		// preOrderLoopMap(root);
		// System.out.println("");
		//
		// System.out.println("pre order loop map");
		// preOrderLoop2(root);
		// System.out.println("");

		// -------------------------------------POSTORDER
		System.out.println("post order recursive");
		postOrderRec(root);
		System.out.println("");
		System.out.println("post order map");
		postOrderLoopMap(root);
		System.out.println("");

	}

}

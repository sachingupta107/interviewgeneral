package sg.problems.backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * Coin change Problem:
 http://www.geeksforgeeks.org/dynamic-programming-set-7-coin-change/

 Given a value N, if we want to make change for N cents, and we have infinite supply of each of S = { S1, S2, .. , Sm} valued coins, how many ways can we make the change? The order of coins doesn�t matter.

 For example, for N = 4 and S = {1,2,3}, there are four solutions: {1,1,1,1},{1,1,2},{2,2},{1,3}. So output should be 4. For N = 10 and S = {2, 5, 3, 6}, there are five solutions: {2,2,2,2,2}, {2,2,3,3}, {2,2,6}, {2,3,5} and {5,5}. So the output should be 5.
 * 
 */
public class CoinChange {
	
	
	private static int minSol = 0;
	private static List<Integer> minSolutionList = null;
	private static List<Integer> currSol = new ArrayList<Integer>();
	
	

	public static int countSolutions(int[] coins, int value) {
		System.out.println("Count Solutions: Coins: " + Arrays.toString(coins) + " value: " + value);
		return countSolutionsRec(coins, (coins.length - 1), value);
	}

	public static int minCoinSolution(int[] coins, int value) {
		minSol = 0;
		minSolutionList = new ArrayList<Integer>();
		currSol = new ArrayList<Integer>();
		System.out.println("Min Coins: Coins: " + Arrays.toString(coins) + " value: " + value);
		return minCoinsRec2(coins, (coins.length - 1), value, 0);
	}

	private static int countSolutionsRec(int[] coins, int max, int value) {
//		System.out.println("Coins: " + Arrays.toString(Arrays.copyOf(coins, max+1)) + " value: " + value);

		if (value < 0)
			return 0;

		if (max < 0)
			return 0;

		if (coins[max] == value)
			return 1 + countSolutionsRec(coins, max - 1, value); // simply drop
																	// the coin
		else
			return countSolutionsRec(coins, max - 1, value) // simply drop the
															// coin
					+ countSolutionsRec(coins, max, value - coins[max]); // use
																			// the
																			// coin

	}

//	private static List<Integer> currSol = new ArrayList<Integer>();
	private static int minCoinsRec2(int[] coins, int max, int value, int lenOfSol) {
//		System.out.println("Coins: " + Arrays.toString(Arrays.copyOf(coins, max+1)) + " Max : "+max+ " value: " + value);
//		System.out.println("currSol: "+currSol);
		if (value < 0)
		{
			//currSol.remove(currSol.size()-1);
			return 0;
		}
			

		if (max < 0)
		{
			//currSol.remove(currSol.size()-1);
			return 0;
		}

		//int ret1 = 0, ret2 = 0; //, minRet = Integer.MAX_VALUE;
		if (coins[max] == value) {
//			System.out.println("Base Case: ");
			lenOfSol++;
			currSol.add(coins[max]);
			
			if (minSol == 0) minSol = lenOfSol;
			
//			System.out.println("length of Sol: "+lenOfSol+" length of minsol: "+minSol);
			
			if (minSol >= lenOfSol) {
				minSol = lenOfSol;
				minSolutionList = new ArrayList<>(currSol);
				System.out.println("currSol: "+currSol + " minsol: " + minSolutionList);
			}
			currSol.clear();
			lenOfSol = 0;
			//ret1 = 1 + 
			//minCoinsRec2(coins, max - 1, value, lenOfSol, currSol); // simply drop the coin
		}
			
			 
		else
		{
			//ret2 = 
			minCoinsRec2(coins, max - 1, value, lenOfSol); // simply drop the coin and recurse
			
//			if ((value - coins[max])  >= coins[max])
			currSol.add(coins[max]);
			minCoinsRec2(coins, max, value - coins[max], lenOfSol + 1); // use the coin and recurse
			
		}
		
//		System.out.println("Coins: " + Arrays.toString(Arrays.copyOf(coins, max+1)) + " value: " + value);
//		System.out.println("ret1: "+ret1+" ret2: "+ret2);
//		return Math.min(ret1, ret2);
//			return minRet;
		//int ret = minSol;
		//minSol = 0;
		return minSol;

	}
	
//	private static List<Integer> changeDetails = new ArrayList<Integer>();
//	private static int minCoins = 0;
//	private static int coinsSoFar = 0;
//	private static int minCoinsRec(int[] coins, int max, int value) {
//		//System.out.println("max: " + max + " value: " + value );
//		System.out.println("Coins: " + Arrays.toString(Arrays.copyOf(coins, max+1)) + " value: " + value);
//
////		if (value == 0) {
////			System.out.println("\nchangeDetails: "+changeDetails+"\n");
////			changeDetails.clear();
////		};
//
//		if (value < 0)
//			return 0;
//
//		if (max < 0)
//			return 0;
//
//		if (value == 0) {
//			System.out.println("Coin use, base case:");
//			//changeDetails.add(coins[max]);
//			System.out.println("\nchangeDetails: "+changeDetails+"\n");
//			changeDetails.clear();
//			//coinsSoFar++;
//			if (coinsSoFar < minCoins) minCoins = coinsSoFar;
//			return 0;
//		}
//		
////		if (coins[max] == value) {
////			System.out.println("Coin use, base case:");
////			changeDetails.add(coins[max]);
////			System.out.println("\nchangeDetails: "+changeDetails+"\n");
////			changeDetails.clear();
////			coinsSoFar++;
////			if (coinsSoFar < minCoins) minCoins = coinsSoFar;
////			return 1;
////		}
//		
//		else {
//			System.out.println("Coin drop:");
//			int ret1 = minCoinsRec(coins, max - 1, value); // simply drop the coin
//			//System.out.println("ret1: "+ret1 );	
//			
//			System.out.println("Coin use:");
//			changeDetails.add(coins[max]);
//			coinsSoFar++;
//			int ret2 = 1 + minCoinsRec(coins, max, value - coins[max]); // use the coin
//			//System.out.println("ret2: "+ret2 );	
//			
//			System.out.println("ret1: "+ret1+" ret2: "+ret2);
//			
//			int ret = 0;
//			
//			if ((ret1 == 0) && (ret2 == 0)) {
//				System.out.println("No Solution");
//				ret = 0;
//			} else if (ret1 == 0 || ret2 == 0) ret =  ret1 + ret2;
//			else ret = Math.min(ret1, ret2);
//			
//			System.out.println("~Ret: "+ ret);
//			return ret;
//		}
//
//	}

	public static void main(String[] args) {
		int[] coins = {2, 5, 10, 25 };
//		int val = 6;
		for (int val = 0; val <= 25; val++) {
			System.out.println("-----");
		int ret = countSolutions(coins, val);
		
		System.out.println("Count Solutions: " + ret);
		System.out.println("   ");
		ret = minCoinSolution(coins, val);
		System.out.println("Min Coin Solutions: " + ret);
		System.out.println("Minsol: "+minSolutionList);
		System.out.println("-----");
	} 
		}
}

package sg.iv.snapdeal;

import java.util.Arrays;

public class NextLargestNumber {
	/*
	 * 2) Find next greater number with same set of digits.
	 * http://www.geeksforgeeks.org/find-next-greater-number-set-digits/
	 */
	/*
	 * Input: n = "218765" Output: "251678"
	 * 
	 * Input: n = "1234" Output: "1243"
	 * 
	 * Input: n = "4321" Output: "Not Possible"
	 * 
	 * Input: n = "534976" Output: "536479"
	 */
	public static void main(String[] args) {
		nextLargest("218765");
		nextLargest("1234");
		nextLargest("4321");
		nextLargest("534976");
	}

	public static String nextLargest(String number) {
		if (number == null || number.length() < 2) {
			System.out.println("Not Possible");
			return number;
		}
		char[] digits = number.toCharArray();
		char digit = digits[digits.length - 1];
		boolean possible = false;
		for (int i = digits.length - 2; i >= 0; i--) {
			if (digits[i] < digit) {
				char temp = digit;
				digits[digits.length - 1] = digits[i];
				digits[i] = temp;
				Arrays.sort(digits, i + 1, digits.length);
				possible = true;
				break;
			}
		}
		if (!possible) {
			System.out.println("Not Possible");
			
		} else {
			System.out.println(digits);
		}
		return digits.toString();
	}

}

package sg.iv.snapdeal;

import sg.ds.btree.Node;
import sg.ds.btree.PrettyPrinterBTree;
import sg.ds.btree.SampleBTrees;
import sg.ds.btree.bst.IntegerBinarySearchTree;

public class CheckBST {

	public static void main(String[] args) {
		IntegerBinarySearchTree bst = new IntegerBinarySearchTree();
		bst.buildTree(10);
		Node<Integer> root = SampleBTrees.test1();
		PrettyPrinterBTree.printBTreeNode(bst.root);
		System.out.println("is BST: "+checkBST(bst.root));
		PrettyPrinterBTree.printBTreeNode(root);
		System.out.println("is BST: "+checkBST(root));
	}
	public static boolean checkBST(Node<Integer> root) {
		//assume all nodes distinct
		if (root!=null) {
			if (root.left==null && root.right==null) return true;
			if (root.left != null && root.right == null) {
				return (minValueBST(root.left) <= root.data) && checkBST(root.left);
			} else if (root.right != null && root.left == null) {
				return  (maxValueBST(root.right) > root.data) && checkBST(root.right);
			} else {
				return  (minValueBST(root.left) <= root.data) && (maxValueBST(root.right) > root.data)
						&& checkBST(root.left) && checkBST(root.right);
			}
		}
		return false;
	}
	
	public static Integer minValueBST(Node<Integer> node) {
		if (node == null) return null;
		if (node.left != null) return minValueBST(node.left);
		return node.data;
		 
	}
	
	public static Integer maxValueBST(Node<Integer> node) {
		if (node == null) return null;
		if (node.right != null) return maxValueBST(node.right);
		return node.data;
		 
	}
}

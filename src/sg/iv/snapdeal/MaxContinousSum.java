package sg.iv.snapdeal;

import java.util.Arrays;

import sg.util.ArrayUtil;

public class MaxContinousSum {
	
	
	public static int maxContinousSum(int[] a) {
		//keep sum so far, change last index only if next value adds to sum 
		//change start index only if current value exceeds all previous sum
		int sumSoFar = 0;
		int maxSumSoFar = a[0];
		int beg = 0;
		int end = a.length;
		for (int i = 0; i < a.length; i++ ) {
//			System.out.println(a[i]);
			
			sumSoFar = sumSoFar + a[i];
			
			maxSumSoFar = Math.max(sumSoFar, maxSumSoFar);
			
			if (sumSoFar == maxSumSoFar) end = i;
			
			if (a[i] > sumSoFar) {
				sumSoFar = a[i];
			}
			if (a[i] > maxSumSoFar) {
				maxSumSoFar = a[i];
				beg = i;
				end = i;
			}
			
		}
		System.out.println("beg: "+beg+" end: "+end);
		return maxSumSoFar;
		
	}
	
	public static int maxContinousSumSolution(int[] arr) {
		int max_ending_here = 0, max_so_far = 0;
		for (int i = 0; i < arr.length; i++ ) {
			int x = arr[i];
			 max_ending_here = Math.max(0, max_ending_here + x);
			 max_so_far = Math.max(max_so_far, max_ending_here);
		}
	       
	    return max_so_far;
	}
	
	    		
	public static void main(String[] args) {
		
		boolean fatgaya = false;
		for (int i = 0; i < 1000; i++) {
			int[] arr = 
			ArrayUtil.getRandomIntArray(5, -10, 10);
			//ArrayUtil.buildIntArrayFromString("0, -7, -9, -7, -2", ",");
			//ArrayUtil.buildIntArrayFromString("4, -7, -7, 2, 7", ",");
			//{1,2,3,-4,-1,9};
			//{-2, -3, 4, -1, -2, 1, 5, -3};
			//{-2, -3, -4, -1, -2, -1, -5, -3};
			System.out.println(Arrays.toString(arr));
			int sum = maxContinousSum(arr);
			int sol = maxContinousSumSolution(arr);
			System.out.println("sum: "+sum);
			System.out.println("sol: "+sol);
			
			if (sum!=sol) {
				if (sol!=0) {
					System.out.println("Fat gaya");  fatgaya = true; break;
				} else {
					int min =  ArrayUtil.getArrayMax(arr);
					System.out.println("min: "+min);
					if (sum != min) {
						System.out.println("Fat gaya");  fatgaya = true; break;
					}
				}
			}
			
		}
		if (!fatgaya) System.out.println("Chal Gaya");
	}
}

package sg.misc;

public class Fibonacci {
	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			System.out.println(fibRec(i+1));
		}
		System.out.println("");
		for (int i = 0; i < 10; i++) {
			System.out.println(fibLoop(i+1));
		}
	}

	public static int fibRec(int n) {
		if (n < 1) return -1;
		if (n==1) return 0;
		if (n==2) return 1;
		int ret = fibRec(n-1) + fibRec(n-2);
		return ret;
	}
	
	public static int fibLoop(int n) {
		if (n < 1) return -1;
		if (n==1) return 0;
		if (n==2) return 1;
		int fib1 = 0, fib2 = 1; int fib = 0;
		for (int i = 3; i <= n; i++) {
			fib = fib1+fib2;
			fib1 = fib2;
			fib2 = fib;
		}
		return fib;
	}
}

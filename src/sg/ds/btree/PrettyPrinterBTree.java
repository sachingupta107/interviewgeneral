package sg.ds.btree;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class BTreePrinterTest {

    public static void main(String[] args) {

        PrettyPrinterBTree.printBTreeNode(SampleBTrees.test1());
        PrettyPrinterBTree.printBTreeNode(SampleBTrees.test2());

    }
}

public class PrettyPrinterBTree {

    public static <T> void printBTreeNode(Node<T> root) {
        int maxLevel = PrettyPrinterBTree.maxLevel(root);

        printBTreeNodeInternal(Collections.singletonList(root), 1, maxLevel);
    }

    private static <T> void printBTreeNodeInternal(List<Node<T>> BTreeNodes, int level, int maxLevel) {
        if (BTreeNodes.isEmpty() || PrettyPrinterBTree.isAllElementsNull(BTreeNodes))
            return;

        int floor = maxLevel - level;
        int endgeLines = (int) Math.pow(2, (Math.max(floor - 1, 0)));
        int firstSpaces = (int) Math.pow(2, (floor)) - 1;
        int betweenSpaces = (int) Math.pow(2, (floor + 1)) - 1;

        PrettyPrinterBTree.printWhitespaces(firstSpaces);

        List<Node<T>> newBTreeNodes = new ArrayList<Node<T>>();
        for (Node<T> BTreeNode : BTreeNodes) {
            if (BTreeNode != null) {
                System.out.print(BTreeNode.data);
                newBTreeNodes.add(BTreeNode.left);
                newBTreeNodes.add(BTreeNode.right);
            } else {
                newBTreeNodes.add(null);
                newBTreeNodes.add(null);
                System.out.print(" ");
            }

            PrettyPrinterBTree.printWhitespaces(betweenSpaces);
        }
        System.out.println("");

        for (int i = 1; i <= endgeLines; i++) {
            for (int j = 0; j < BTreeNodes.size(); j++) {
                PrettyPrinterBTree.printWhitespaces(firstSpaces - i);
                if (BTreeNodes.get(j) == null) {
                    PrettyPrinterBTree.printWhitespaces(endgeLines + endgeLines + i + 1);
                    continue;
                }

                if (BTreeNodes.get(j).left != null)
                    System.out.print("/");
                else
                    PrettyPrinterBTree.printWhitespaces(1);

                PrettyPrinterBTree.printWhitespaces(i + i - 1);

                if (BTreeNodes.get(j).right != null)
                    System.out.print("\\");
                else
                    PrettyPrinterBTree.printWhitespaces(1);

                PrettyPrinterBTree.printWhitespaces(endgeLines + endgeLines - i);
            }

            System.out.println("");
        }

        printBTreeNodeInternal(newBTreeNodes, level + 1, maxLevel);
    }

    private static void printWhitespaces(int count) {
        for (int i = 0; i < count; i++)
            System.out.print(" ");
    }

    private static <T> int maxLevel(Node<T> BTreeNode) {
        if (BTreeNode == null)
            return 0;

        return Math.max(PrettyPrinterBTree.maxLevel(BTreeNode.left), PrettyPrinterBTree.maxLevel(BTreeNode.right)) + 1;
    }

    private static <T> boolean isAllElementsNull(List<T> list) {
        for (Object object : list) {
            if (object != null)
                return false;
        }

        return true;
    }

}
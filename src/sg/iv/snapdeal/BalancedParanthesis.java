package sg.iv.snapdeal;

import java.util.Stack;

public class BalancedParanthesis {
	public static void main(String[] args) {
		checkBalance("(()){}()");
	}
	public static boolean checkBalance(String brackets) {
		char[] bracketChars = brackets.toCharArray();
		Stack<Character> stk = new Stack<>();
		for (int i = 0; i < bracketChars.length; i++) {
			if (bracketChars[i] == '(') {
				stk.push(bracketChars[i]);
			} else if (bracketChars[i] == ')') {
				if (stk.isEmpty()) {
					System.out.println("Mismatch"); return false;
				} else {
					char b = stk.pop();
					if (b != '(') {
						System.out.println("Mismatch"); return false;
					}
				}
			}
		}
		if (!stk.isEmpty()) {
			System.out.println("Mismatch"); 
			return false;
		} else {
			System.out.println("Match");
			return true;
		}
	}
}

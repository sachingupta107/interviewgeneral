package sg.iv.snapdeal;

import java.util.concurrent.atomic.AtomicBoolean;

public class ThreadHelloWorld {
	private static AtomicBoolean alt = new AtomicBoolean(true);
	private static AtomicBoolean stop = new AtomicBoolean(false);

	public static void main(String[] args) {
		Thread t1 = new Thread(new Runnable() {

			@Override
			public void run() {
				while (!stop.get())
					if (alt.get()) {
						System.out.print("Hello ");
						alt.set(false);
					}

			}

		});

		Thread t2 = new Thread(new Runnable() {

			@Override
			public void run() {
				while (!stop.get())
					if (!alt.get()) {
						System.out.println("World");
						alt.set(true);
					}

			}

		});
		t1.start();
		t2.start();
		
	}
}

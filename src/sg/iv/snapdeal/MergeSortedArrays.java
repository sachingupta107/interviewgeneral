package sg.iv.snapdeal;

import java.util.Arrays;

import sg.util.ArrayUtil;

public class MergeSortedArrays {
	public static void main(String[] args) {
		int[] arr1 = ArrayUtil.getRandomIntArray(10, 100);
		int[] arr2 = ArrayUtil.getRandomIntArray(10, 100);
		Arrays.sort(arr1);
		Arrays.sort(arr2);
		ArrayUtil.printIntArray(arr1);
		ArrayUtil.printIntArray(arr2);
		int[] arr = mergeSoredArrays(arr1, arr2);
		System.out.println("output:");
		ArrayUtil.printIntArray(arr);
	}
	public static int[] mergeSoredArrays(int[] arr1, int[] arr2) {
		if (arr1==null || arr2==null) return null;
		int[] arr = new int[arr1.length+arr2.length];
		for (int i = 0; i < arr1.length ; i++) {
			arr[i] = arr1[i];
		}
		for (int i = arr1.length; i < arr.length ; i++) {
			arr[i] = 0;
		}
		
		ArrayUtil.printIntArray(arr);
		
		int filledLen = arr1.length - 1;
		int j = filledLen;
		
//		System.out.println("j: "+j);
		
		for (int i = 0 ; i < arr2.length; i++) {
//			System.out.println("i: "+i);
			while ((j >= 0) && (arr[j] > arr2[i])) {
//				System.out.println("	j: "+j);
				arr[j+1]=arr[j];
				j--;	
			}
			j++;
			arr[j] = arr2[i];
			filledLen++;
			j = filledLen;
//			ArrayUtil.printIntArray(arr);
		}
		return arr;
	}
}

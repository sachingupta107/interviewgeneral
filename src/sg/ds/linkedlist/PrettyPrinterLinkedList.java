package sg.ds.linkedlist;

public class PrettyPrinterLinkedList {
	
	public static <T> void printLinkedList(Node<T> root) {
		if (root == null) {
			System.out.println("Null List");
			return;
		}
		
		Node<?> node = root;
		//System.out.println("");
		while (node != null) {
			System.out.print(node.data);
			if (node.next != null)
			System.out.print(" -> ");
			node = node.next;
		}
		System.out.println("");
	}
	
	public static void main(String[] args) {
		PrettyPrinterLinkedList.printLinkedList(LinkedListBuilder.getIntegerList(10, 100));
	}
}
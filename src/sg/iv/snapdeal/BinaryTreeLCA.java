package sg.iv.snapdeal;

import sg.ds.btree.Node;
import sg.ds.btree.PrettyPrinterBTree;
import sg.ds.btree.SampleBTrees;

public class BinaryTreeLCA {
	
	public static void main(String[] args) {
		Node<Integer> root = SampleBTrees.test3();
		PrettyPrinterBTree.printBTreeNode(root);
		System.out.println(findLCABinaryTree(root, new Node<Integer>(4), new Node<Integer>(60)));
	}
	
	public static Node<Integer> findLCABinaryTree(Node<Integer> root, Node<Integer> node1, Node<Integer> node2) {
		System.out.println("BEG: root: "+root+" node1: "+node1+" node2: "+node2);
		if (root == null) return null;
		
//		if (root.left != null && root.left.data == node1.data  && root.right != null && root.right.data == node2.data) {
//			return root;
//		} 
		
		if (root.data == node1.data || root.data == node2.data) {
			return root;
		}
		
		Node<Integer> lcaLeft = findLCABinaryTree(root.left, node1, node2);
		Node<Integer> lcaRight = findLCABinaryTree(root.right, node1, node2);
		
		System.out.println("---> root: "+root+" lcaLeft: "+lcaLeft+" lcaRight: "+lcaRight);
		
		if (lcaLeft != null && lcaRight != null) return root;
		
		if (lcaLeft != null) return lcaLeft; //findLCABinaryTree(root.left, node1, node2);
		if (lcaRight != null) return lcaRight; //findLCABinaryTree(root.right, node1, node2);
		System.out.println("END: root: "+root+" lcaLeft: "+lcaLeft+" lcaRight: "+lcaRight);
		return null;
	}
	public static Node<Integer> findLCA_BST(Node<Integer> root, Node<Integer> node1, Node<Integer> node2) {
		return null;
	}
	

}

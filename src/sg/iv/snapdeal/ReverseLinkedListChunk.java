package sg.iv.snapdeal;

import sg.ds.linkedlist.LinkedListBuilder;
import sg.ds.linkedlist.Node;
import sg.ds.linkedlist.PrettyPrinterLinkedList;

public class ReverseLinkedListChunk {
	public static void main(String[] args) {
//		Node<Integer> head = LinkedListBuilder.getIntegerList(5, 100);
		Node<Integer> head = LinkedListBuilder.buildIntegerListFromString("1 -> 2 -> 3 -> 4 -> 5 -> 6", "->");
		PrettyPrinterLinkedList.printLinkedList(head);
		head = revChunk(head, 5);
		System.out.println("Output: ");
		PrettyPrinterLinkedList.printLinkedList(head);
	}

	public static <T> Node<T> revChunk(Node<T> head, int chunk) {

		if (head == null)
			return null;

		Node<T> prevTail = null;
		Node<T> currHead = head;
		
		//set head
//		for (int i = 0; i < 2; i++) {
//			head = head.next;
//		}
//		System.out.println(head);
		
		
		
		
		
		while (currHead != null) {
			
			//init variable for next set reversal;
			Node<T> prev = null;
			Node<T> curr = currHead;
			
//			System.out.println("prev: "+prev);
//			System.out.println("curr: "+curr);
			
			for (int i = 0; i < chunk; i++) {
				Node<T> temp = curr.next;
				
				curr.next = prev;
				prev = curr;
				curr = temp;
				if (curr == null) break;

			}
			
//			System.out.println("prev: "+prev);
//			System.out.println("curr: "+curr);
//			System.out.println("prevTail: "+prevTail);
//			System.out.println("currHead: "+currHead);
			
			//wire set reversed to prev set
			if (prevTail != null) {
				prevTail.next = prev;
			} else {
				head = prev;
			}
			
			prevTail = currHead;
			currHead = curr;
			
			System.out.println(head);

			
		}
		return head;
		
	}
}

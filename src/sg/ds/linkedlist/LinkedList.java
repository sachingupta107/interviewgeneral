package sg.ds.linkedlist;

public class LinkedList<T> {
	
	private Node<T> root;

	

	public void reverseRec() {
		reverseRec(null, root);
	}

	private void reverseRec(Node<T> prev, Node<T> curr) {
		if (curr == null && prev != null) {
			root = prev;
			return;
		}

		Node<T> temp = curr.next;
		curr.next = prev;
		prev = curr;
		curr = temp;

		reverseRec(prev, curr);

	}

	public void reverse() {

		Node<T> prev = null;
		Node<T> node = root;

		while (node != null) {
			Node<T> temp = node.next;
			node.next = prev;
			prev = node;
			node = temp;
		}

		root = prev;
	}

//	public void insertSorted(int data) {
//
//		Node<Integer> node = new Node<>(data);
//		if (root == null) {
//			root = node;
//			return;
//		}
//
//		Node prev = null;
//		Node curr = root;
//
//		while (curr != null) {
//			if (curr.data >= node.data) {
//				if (prev == null) {
//					node.next = root;
//					root = node;
//				} else {
//					prev.next = node;
//					node.next = curr;
//				}
//				return;
//			}
//			prev = curr;
//			curr = curr.next;
//		}
//		prev.next = node;
//		node.next = null;
//	}

//	public void swapNodesByData(int data1, int data2) {
//		Node prev1 = null;
//		Node prev2 = null;
//
//		Node tmp = root;
//		while (tmp != null) {
//			if (tmp.data == data1) {
//				break;
//			}
//			prev1 = tmp;
//			tmp = tmp.next;
//		}
//
//		tmp = root;
//		while (tmp != null) {
//			if (tmp.data == data2) {
//				break;
//			}
//			prev2 = tmp;
//			tmp = tmp.next;
//		}
//
//		Node node1 = prev1.next;
//		Node node2 = prev2.next;
//
//		Node node1Next = node1.next;
//		System.out.println("node1Next: " + node1Next);
//
//		prev1.next = node2;
//		prev2.next = node1;
//
//		node1Next = node1.next;
//		System.out.println("node1Next: " + node1Next);
//
//		node1.next = node2.next;
//		node2.next = node1Next;
//
//	}

	public static void main(String[] args) {
		LinkedList<Integer> list1 = new LinkedList<>();
		list1.root = LinkedListBuilder.getIntegerList();
		PrettyPrinterLinkedList.printLinkedList(list1.root);

		list1.reverse();
		PrettyPrinterLinkedList.printLinkedList(list1.root);

		list1.reverseRec();
		PrettyPrinterLinkedList.printLinkedList(list1.root);

		// list1.buildListFromString("1 2 3 4 5");
		// list1.printList();
		//
		// list1.swapNodesByData(2, 4);
		// list1.printList();
		//
		// list1.buildListFromString("1 2 3 4 5");
		// list1.swapNodesByData(2, 3);
		// list1.printList();
		//
		// list1.buildListFromString("1 2 3 4 5");
		// list1.swapNodesByData(2, 2);
		// list1.printList();

	}
}

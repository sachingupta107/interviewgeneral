package sg.ds.btree;

public class Node<T> {
	
	public Node<T> left, right;
    public T data;

    public Node(T data) {
        this.data = data;
    }

	@Override
	public String toString() {
		//return "[left=" + (left != null) + ", right=" + (right != null) + ", data=" + data+ "]";
		return "[" + data+ "]";
	}

}

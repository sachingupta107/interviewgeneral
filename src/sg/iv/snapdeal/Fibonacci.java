package sg.iv.snapdeal;

import java.util.Arrays;

public class Fibonacci {
	public static void main(String[] args) {
		fibSeries(10);
		fibRec(10);
		fibRec2(10, true);
		System.out.println("Memoize 0: "); fibMemoize(0, true);
		System.out.println("Memoize 1: "); fibMemoize(1, true);
		System.out.println("Memoize 2: "); fibMemoize(2, true);
		System.out.println("Memoize 10: "); fibMemoize(10, true);
	}
	
	public static void fibSeries(int n) {
		if (n < 1) return;
		if (n==1) {
			System.out.println(0);
			return;
		}
		
		int prev = 0;
		int curr = 1;
		
		int count = 1;
		System.out.println(count+":"+0);
		while (count < n) {
			count++;
			System.out.println(count+":" + curr);
			int temp = curr;
			curr = prev+curr;
			prev = temp;
		}
	}
	
	public static void fibRec(int n) {
		if (n==1) System.out.println("0"); 
		else if (n>=2)  fibRec(n-2, 0, 1);
	}
	
	public static void fibRec2(int n, boolean printSerires) {
		if (!printSerires) {
			int fib = fibRec2(n);
			System.out.println(fib);
		}
		else {
			for (int i = 1; i <= n; i++) {
				int fib = fibRec2(i);
				System.out.println(fib);
			}
		}
	}
	
	public static void fibMemoize(int n, boolean printSerires) {
		if (n < 1) return;
		int[] fibSeries = new int[n];
		Arrays.fill(fibSeries, -1);
		if (!printSerires) {
			int fib = fibMemoize(n, fibSeries);
			System.out.println(fib);
		}
		else {
			for (int i = 1; i <= n; i++) {
				int fib = fibMemoize(i, fibSeries);
				System.out.println(fib);
			}
		}

		fibMemoize(n, fibSeries);
	}
	private static void fibRec(int n, int prev, int curr) {
		//
		if (n==0) {
			System.out.println(curr);
			return;
		} else {
			int temp = curr;
			curr = curr + prev;
			prev = temp;
			fibRec(n-1, prev, curr);
		}
		
	}
	
	private static int fibRec2(int n) {
		//
		if (n < 1) return -1;
		if (n==1) {
			//System.out.println(0);
			return 0;
		} else if (n == 2) {
			//System.out.println(1);
			return 1;
		} else {
			int fib = fibRec2(n - 1) + fibRec2(n - 2);
			//System.out.println(fib);
			return fib;
		}
		
	}
	
	private static int fibMemoize(int n, int[] fibSeries) {
		
		if (n < 1) return -1;
		if (n==1) {
			//System.out.println(0);
			fibSeries[0] = 0;
			return fibSeries[0];
		} else if (n == 2) {
			//System.out.println(1);
			fibSeries[1] = 1;
			return 1;
		} else if (fibSeries[n-1] > 0) 
			return fibSeries[n-1];
		else {
			int fib = fibRec2(n - 1) + fibRec2(n - 2);
			//System.out.println(fib);
			fibSeries[n-1] = fib;
			return fib;
		}
		
	}
}

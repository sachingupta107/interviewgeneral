package sg.iv.snapdeal;

public class ReverseString {
 public static void main(String[] args) {
	

	 
	 String ret = revString("loophello");
		System.out.println(ret);
		
	  ret = revStringRec("recursivehello");
		System.out.println(ret);
//		System.out.println("hello".substring(0, "hello".length()));
		
}
 
 public static String revString(String str) {
		StringBuilder sb = new StringBuilder();
		sb.append(str);
		for (int i = 0; i < str.length()/2; i++) {
			char f = str.charAt(i);
			char e = str.charAt(str.length() - (i+1));
			sb.setCharAt(str.length() - (i+1), f);
			sb.setCharAt(i, e);
//			System.out.println(sb.toString());
		}
		return sb.toString();
	}
 
 public static String revStringRec(String str) {
		if (str.length() <= 1) return str;
		return  str.charAt(str.length()-1) + revStringRec(str.substring(1, str.length()-1)) + str.charAt(0); 
	}
}

